<?php
// Iniciar sesión si aún no está iniciada
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Aquí puedes verificar si hay alguna variable de sesión específica que quieras usar para confirmar la compra

?>

    <h1>Compra Realizada con Éxito</h1>
    <p>Tu compra ha sido procesada correctamente. ¡Gracias por tu pedido!</p>
    <a href="index.php">Volver a la página principal</a>

