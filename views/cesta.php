
<div class="container-cart">
    <div class="cart">
        <h2 class="carrito">Carrito de Compras</h2>
        
        <?php
        include_once 'model/carrito.php'; // Incluye la definición de la clase Carrito

         if (!empty($articulosEnCarrito)): ?>
            <?php foreach ($articulosEnCarrito as $articulo): ?>
                <div class="product">
                    <div class="product-title"><?= htmlspecialchars($articulo->getNombre()) ?></div>
                    <div class="product-price"><?= htmlspecialchars($articulo->getPrecio()) ?></div>
                    <img class="product-img" src="img/<?= htmlspecialchars($articulo->getImg() ?: 'default.jpg') ?>" alt="Product Image">
                    <a href="index.php?controller=Carrito&action=delete&id=<?= htmlspecialchars($articulo->getIdarticulos()) ?>" class="btn btn-danger">Eliminar <i class="fa-solid fa-trash-can"></i></a>

                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p>No hay artículos en el carrito.</p>
        <?php endif; ?>
    </div>

    <div class="order-summary">
        <div class="order-summary-title">Order summary</div>
        <!-- Suponiendo que $listacompras tiene los detalles del pedido actual -->
        <?php if (!empty($listacompras)): ?>
            <?php foreach ($listacompras as $compras): ?>
                <div class="shipping">Shipping: <?= htmlspecialchars($compras->getCostoEnvio()); ?></div>
                <div class="precio_total">Total: <?= htmlspecialchars($compras->getPrecioTotal()); ?></div>
                <div class="fecha_compra">Date: <?= htmlspecialchars($compras->getFechaCompra()); ?></div>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- Formulario para continuar al pago -->
        <form action="#" method="POST">
            <button class="continue-btn" type="submit">Continue to payment</button>
        </form>
    </div>
</div>
