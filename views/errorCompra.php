<?php
// Iniciar sesión si aún no está iniciada
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Aquí puedes manejar errores específicos o mensajes pasados a través de la sesión o query parameters

?>

    <h1>Error al Procesar la Compra</h1>
    <p>Ha ocurrido un error al procesar tu compra. Por favor, intenta de nuevo más tarde.</p>
    <a href="carrito.php">Volver al Carrito</a>

