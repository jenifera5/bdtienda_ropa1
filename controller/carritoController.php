<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include_once 'model/compras.php';
include_once 'model/articuloDAO.php';
include_once 'model/carrito.php';

class CarritoController {

    public function list() {
        
        if (!isset($_SESSION['id_usuario'])) {
            // Si el usuario no ha iniciado sesión, redirigirlo a la página de inicio de sesión
            header("Location: login.php");
            exit();
        }
        
        $listaarticulos = ArticuloDAO::getAllArticulos();
        $listacompras = ComprasDAO::getAllCompras();
       
        // Verifica si el carrito existe en la sesión, si no, inicialízalo.
        if (!isset($_SESSION['carrito'])) {
            $_SESSION['carrito'] = new Carrito(); // Asegúrate de que Carrito tiene un constructor adecuado
        }
        $articulosEnCarrito = $_SESSION['carrito']->obtenerArticulos();
        
        $compra = new Compras();
        $compra->setPrecioTotal(isset($_POST['precio_total']) ? $_POST['precio_total'] : 0);
        $compra->setFechaCompra(date("Y-m-d H:i:s"));
        $compra->setCostoEnvio(isset($_POST['costo_envio']) ? $_POST['costo_envio'] : 0);
        $compras = $compra; 
        $view = 'views/cesta.php';
        include_once 'views/main.php';
    }
    
    

   
public function add() {
   
    if (isset($_GET['id'])) {
        $idarticulos = $_GET['id'];

        $articulo = ArticuloDAO::getArticuloByID($idarticulos);
        
        if ($articulo) {
            $_SESSION['carrito']->agregarArticulo($articulo);
        }
        
    }
    
    
    // Redirigir al usuario de vuelta a la página anterior
    header("Location: {$_SERVER['HTTP_REFERER']}");
    exit();
}


public function delete() {
    if(isset($_GET['id'])){
        $idarticulos = $_GET['id'];
        if (!isset($_SESSION['carrito'])) {
            $_SESSION['carrito'] = new Carrito(); // Asegúrate de inicializar si aún no existe
        }
        $carrito = $_SESSION['carrito'];
        $carrito->eliminarArticulo($idarticulos);
        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit();
    }
}
// Añade este método al CarritoController si aún no existe
public function handleRequest() {
    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            case 'agregarArticulo':
                $this->agregarArticulo();
                break;
            case 'eliminarArticulo':
                $this->eliminarArticulo();
                break;
            // Añade más casos según sea necesario
        }
    }
}



}
// Luego, al final del archivo CarritoController.php, después de la definición de la clase, añade:
$controller = new CarritoController();
$controller->handleRequest();
?>

