<?php
include_once 'model/comprasDAO.php';
include_once 'model/articuloDAO.php';
include_once 'model/carrito.php';

// Asegúrate de que la sesión se inicia en el script que incluye este controlador o en el punto de entrada de la aplicación.
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class ComprasController {
    // Eliminado el constructor

    public function procesarCompra() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Asegúrate de que la sesión se haya iniciado
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }

            // Asegúrate de que hay un carrito en la sesión
            $carrito = $_SESSION['carrito'] ?? new Carrito();

            // Crear una instancia de Compras
            $compra = new Compras();
            
            // Calcula el precio total de los artículos en el carrito
            $precioTotal = 0;
            foreach ($carrito->obtenerArticulos() as $articulo) {
                $precioTotal += $articulo->getPrecio(); // Asume que Articulo tiene un método getPrecio()
            }
            
            // Asigna el precio total y la fecha actual
            $compra->setPrecioTotal($precioTotal);
            $compra->setFechaCompra(date("Y-m-d H:i:s"));
            $compra->setCostoEnvio(isset($_POST['costo_envio']) ? floatval($_POST['costo_envio']) : 0);
            $compra->setIdUsuario($_SESSION['id_usuario'] ?? null); // Asume que el ID del usuario está en la sesión
            
            // Realiza la compra
            $comprasDAO = new ComprasDAO();
            $resultado = $comprasDAO->realizarCompra($compra);

            if ($resultado) {
                // Limpia el carrito después de la compra
                $carrito->vaciarCarrito();
                $_SESSION['carrito'] = $carrito; // Actualiza la sesión con el carrito vacío
                
                // Redirige al usuario a la página de confirmación de compra
                header("Location: confirmacionCompra.php");
                exit;
            } else {
                // Redirige al usuario a la página de error de compra
                header("Location: errorCompra.php");
                exit;
            }
        }
    }
}
?>


