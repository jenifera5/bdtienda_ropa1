<?php
class Carrito {
    private $articulos;
 
    public function __construct() {
        $this->articulos = [];
    }

    public function agregarArticulo($articulo) {
        $this->articulos[] = $articulo;
    }

    public function obtenerArticulos() {
        return $this->articulos;
    }

    public function eliminarArticulo($idArticulos) {
        // Filtrar los artículos para eliminar el que tiene el ID especificado
        $this->articulos = array_filter($this->articulos, function($articulo) use ($idArticulos) {
            // Suponiendo que el artículo tiene un método getId() para obtener su ID
            return $articulo->getIdarticulos() != $idArticulos;
        });

        // Reindexar el array para evitar índices vacíos después de la eliminación
        $this->articulos = array_values($this->articulos);
    }
}
?>